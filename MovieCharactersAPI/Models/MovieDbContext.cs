﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models.Domain;
using System.Diagnostics.CodeAnalysis;

namespace MovieCharactersAPI.Models
{
    public class MovieDbContext : DbContext
    {
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //seed data

            // Characters
            modelBuilder.Entity<Character>().HasData(new Character { CharacterId = 1, CharacterName = "Tim", CharacterAlias = "Tim the timable", CharacterPicture = "bilde.url", Gender = "Male"});

            // Movies
            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 1, MovieTitle = "Tim til nye høyder", Director = "Einstein", Trailer = "youtube.com", Genre = "Skrekk", MoviePicture = "Bilde.com", ReleaseYear = 2002 });
            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 2, MovieTitle = "Tim til nye høyder2", Director = "Einstein", Trailer = "youtube.com", Genre = "Skrekk", MoviePicture = "Bilde.com", ReleaseYear = 2003 });

            // Franchise
            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 1, FranchiseName = "Tim sine eventyr", FranchiseDescription = "Tim drar på eventyr" });


            modelBuilder.Entity<Character>()
                .HasMany(p => p.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 1, MovieId = 2 }
                        );
                    });
        }
    }
}
