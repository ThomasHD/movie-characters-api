﻿namespace MovieCharactersAPI.Models.Domain
{
    public class Franchise
    {
        //PK
        public int FranchiseId { get; set; }

        // Fields
        public string FranchiseName { get; set; }
        public string FranchiseDescription { get; set; }

        //Realations 
        public ICollection<Movie>? Movies { get; set; }

    }
}
