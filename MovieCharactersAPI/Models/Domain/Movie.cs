﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Models.Domain
{
    [Table("Movie")]
    public class Movie
    {
        //PK
        public int MovieId { get; set; }

        // Fields
        //[Required]
        //[MaxLength(50)]

        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string MoviePicture { get; set; }
        public string Trailer { get; set; }

        // Relationships
        public ICollection<Character> Characters { get; set; }
        public int? FranchiseId { get; set; }

        public Franchise? Franchise { get; set; }




    }
}
