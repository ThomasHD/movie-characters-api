﻿namespace MovieCharactersAPI.Models.Domain
{
    public class Character
    {
        //PK
        public int CharacterId { get; set; }

        //Fields
        public string CharacterName { get; set; }
        public string CharacterAlias { get; set; }
        public string Gender { get; set; }
        public string CharacterPicture { get; set; }

        public ICollection<Movie>? Movies { get; set; }




    }
}
